Guía práctica de GIT

1) Instala Git en su ambiente de desarrollo Respuesta: Erick Padilla@Erick MINGW64 ~/Videos/proyecto1erick (master) $ git version git version 2.28.0.windows.1

2) Crea una cuenta gratuita en Bitbucket Respuesta: https://bitbucket.org/mcneilus31/

3) Crea un repositorio en Bitbucket para utilizarlo en las sucesivas prácticas del curso Respuesta: https://bitbucket.org/mcneilus31/proyecto1erick/src/master/

4) Crea un repositorio en tu ambiente de desarrollo y vincularlo al repositorio de Bitbucket Respuesta:

a) Erick Padilla@Erick MINGW64 ~/Videos
$ mkdir proyecto-prueba

b) Erick Padilla@Erick MINGW64 ~/Videos
$ cd proyecto-prueba

c) Erick Padilla@Erick MINGW64 ~/Videos/proyecto-prueba
$ pwd
/c/Users/Pavilion/Videos/proyecto-prueba

d) Erick Padilla@Erick MINGW64 ~/Videos/proyecto-prueba
$ git init
Initialized empty Git repository in C:/Users/Pavilion/Videos/proyecto-prueba/.git/

e) Erick Padilla@Erick MINGW64 ~/Videos/proyecto-prueba (master)
$ git remote add origin https://bitbucket.org/mcneilus31/proyecto1erick/src/master/

f) Erick Padilla@Erick MINGW64 ~/Videos/proyecto-prueba (master)
$ git config -e

    [core]
        repositoryformatversion = 0
        filemode = false
        bare = false
        logallrefupdates = true
        symlinks = false
        ignorecase = true
    [remote "origin"]
        url = https://bitbucket.org/mcneilus31/proyecto1erick/src/master/
        fetch = +refs/heads/*:refs/remotes/origin/*


g) Erick Padilla@Erick MINGW64 ~/Videos/proyecto-prueba (master)
    $ git pull
    remote: Counting objects: 16, done.
    remote: Compressing objects: 100% (13/13), done.
    remote: Total 16 (delta 3), reused 0 (delta 0)
    Unpacking objects: 100% (16/16), 2.94 KiB | 4.00 KiB/s, done.
    From https://bitbucket.org/mcneilus31/proyecto1erick/src/master
    * [new branch]      master     -> origin/master
    There is no tracking information for the current branch.
    Please specify which branch you want to merge with.
    See git-pull(1) for details.

        git pull <remote> <branch>

    If you wish to set tracking information for this branch you can do so with:

        git branch --set-upstream-to=origin/<branch> master


h) Erick Padilla@Erick MINGW64 ~/Videos/proyecto-prueba (master)
    $ git pull origin master
    From https://bitbucket.org/mcneilus31/proyecto1erick/src/master
    * branch            master     -> FETCH_HEAD


i)Erick Padilla@Erick MINGW64 ~/Videos/proyecto-prueba (master)
$ git log
    commit fc1a6646c037bc275cea28658f2bd4291901b862 (HEAD -> master, origin/master)
    Author: Erick Padilla Maldonado <erickjpm2012zte@gmail.com>
    Date:   Sat Oct 17 15:25:12 2020 +0000

        proyecto1erick edited online with Bitbucket

    commit a01b7735a3d9d656b642c953ba158bcee285b575
    Author: Erick Padilla Maldonado <erickjpm2012zte@gmail.com>
    Date:   Sat Oct 17 15:25:04 2020 +0000

    .gitignore edited online with Bitbucket

    commit 85ebf1f3e134a4ae9c648d74a109c140e764d954
    Author: Erick Padilla Maldonado <erickjpm2012zte@gmail.com>
    Date:   Sat Oct 17 15:24:55 2020 +0000

        README.md edited online with Bitbucket

    commit 3bf007cfc53df3debf9d616fe6fe6c0a12439ef1
    Author: Erick Padilla Maldonado <erickjpm2012zte@gmail.com>
    Date:   Sat Oct 17 04:31:30 2020 +0000

        readme edited online with Bitbucket

    commit 5b3a78a25b13cdb33ed05fba650159f17b3b7e90
    Author: Erick Padilla Maldonado <erickjpm2012zte@gmail.com>
    Date:   Fri Oct 16 22:57:42 2020 -0500

        Primer repositorio

    commit 908af74878466355cea275e1e38b8264713ced73
    Author: Erick Padilla Maldonado <erickjpm2012zte@gmail.com>
    Date:   Wed Oct 14 06:15:19 2020 +0000
    :
5) Como paso final, crea un archivo del estilo README (sólo texto) y súbelo via línea de comandos al repositorio de Bitbucket creado previamente

Realizado !!!